FROM node:lts-alpine

# install flyway
ENV FLYWAY_VERSION 6.5.6
ENV PATH="/flyway:${PATH}"
WORKDIR /flyway
RUN apk update && apk add --no-cache git bash && apk upgrade
RUN apk add openjdk12-jre --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ --allow-untrusted && \
    apk --no-cache add curl
RUN curl -L https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/${FLYWAY_VERSION}/flyway-commandline-${FLYWAY_VERSION}.tar.gz -o flyway-commandline-${FLYWAY_VERSION}.tar.gz \
  && tar -xzf flyway-commandline-${FLYWAY_VERSION}.tar.gz --strip-components=1 \
  && rm flyway-commandline-${FLYWAY_VERSION}.tar.gz

#install babel
RUN npm install -g @babel/core @babel/cli @babel/node

# setup app
ENV NODE_ENV=production
WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install --production
COPY . .
RUN npm run build
CMD [ "npm", "run", "start:prod" ]
