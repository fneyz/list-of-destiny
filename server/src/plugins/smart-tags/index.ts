import { makeJSONPgSmartTagsPlugin } from 'graphile-utils';

export const getSmartTagPlugin = () => makeJSONPgSmartTagsPlugin({
    version: 1,
    config: {
        class: {
            flyway_schema_history: {
                tags: {
                    omit: 'read,update,create,delete,all,many',
                },
            }
        }
    }
});
