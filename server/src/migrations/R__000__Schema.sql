create extension if not exists "uuid-ossp";

create table if not exists note
(
    id uuid primary key,
    title text,
    body text
)