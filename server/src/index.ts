import express from 'express';
import { postgraphile } from 'postgraphile';
import { getSmartTagPlugin } from './plugins';

const app = express();
const config = {
    watchPg: true,
    graphiql: true,
    enhanceGraphiql: true,
    appendPlugins: [
        getSmartTagPlugin()
    ],
};

app.use(
    postgraphile(
      process.env.DATABASE_URL || "postgres://postgres:postgres@localhost:5432/postgres",
      "public",
      config
    )
);

app.listen(process.env.PORT || 3000);